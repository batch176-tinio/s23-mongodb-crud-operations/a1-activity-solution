// Create

db.users.insertMany([
	{	
		"firstName": "John",
		"lastName": "Lennon",
		"email": "jlennon@gmail.com",
		"password": "jlennon123",
		"isAdmin": false

	},
	{	
		"firstName": "Paul",
		"lastName": "Mccartney",
		"email": "pmccartney@gmail.com",
		"password": "pmccartney123",
		"isAdmin": false

	},
	{	
		"firstName": "George",
		"lastName": "Harrison",
		"email": "gharrison@gmail.com",
		"password": "gharrison123",
		"isAdmin": false

	},
	{	
		"firstName": "Richard",
		"lastName": "Starsky",
		"email": "rstarsky@gmail.com",
		"password": "rstarsky123",
		"isAdmin": false

	},
	{	
		"firstName": "Pete",
		"lastName": "Best",
		"email": "pbest@gmail.com",
		"password": "pbest123",
		"isAdmin": false

	}

])



db.newCourses.insertMany([
	{	
		"name": "HTML 101",
		"price": 1000,
		"isActive": false
	},
	{	
		"name": "CSS 101",
		"price": 2000,
		"isActive": false
	},
	{	
		"name": "JavaScript 101",
		"price": 3000,
		"isActive": false
	}

])


// Read

db.users.find({"isAdmin": false})


// Update

db.users.updateOne({}, {$set:{"isAdmin":true}})
db.newCourses.updateOne({"price": 3000}, {$set:{"isActive":true}})


// Delete
db.newCourses.deleteMany({"isActive":false})










